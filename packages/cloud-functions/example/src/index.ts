import { Request, Response } from 'express'

const example = (req: Request, res: Response) => {
    res.json({ hello: 'world' })
}

export { example }